import { createElement } from '../helpers/domHelper';
import { fighterService } from '../services/fightersService';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const characters = createElement({
    tagName: 'div',
    className: 'character-box'
  })

  if (fighter) {

    const fighterImg = createFighterImage(fighter)
    fighterElement.append(fighterImg)
    fighterElement.append(characters)

    characters.innerHTML = `<div>
      <div class='title'> ${fighter.name} </div>
      <div> Health: ${fighter.health} </div>
      <div> Attack: ${fighter.attack} </div>
      <div> Defense: ${fighter.defense} </div>
    </div>`

    if (fighterElement.classList.contains('fighter-preview___right')) {
          fighterImg.classList.toggle('img-right');
    } else {
          fighterImg.classList.toggle('img-left');
    }
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
