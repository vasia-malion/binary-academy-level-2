import { controls } from '../../constants/controls';

let firstFighterSuperAttackAvailable = true;
let secondFighterSuperAttackAvailable = true;

export async function fight(firstFighter, secondFighter) {

  const { health: firstFighterHealth} = firstFighter;
  const { health: secondFighterHealth} = secondFighter;

  let firstFighterBlockAvailable = false;
  let secondFighterBlockAvailable = false;

  const firstFighterLineHealth = document.getElementById('left-fighter-indicator');
  const secondFighterLineHealth = document.getElementById('right-fighter-indicator');

  return new Promise((resolve) => {
    if (firstFighterSuperAttackAvailable) {
      useSuperAttackPress( () => useSuperAttack(
        firstFighter,
        secondFighter,
        secondFighterHealth,
        secondFighterLineHealth,
        1),
        ...controls.PlayerOneCriticalHitCombination
      )
    }

    if (secondFighterSuperAttackAvailable) {
      useSuperAttackPress( () => useSuperAttack(
        secondFighter,
        firstFighter,
        firstFighterHealth,
        firstFighterLineHealth,
        2),
        ...controls.PlayerTwoCriticalHitCombination)
    }

    function isWinner () {
      if (firstFighter.health <= 0 ) {
        resolve(secondFighter)
      } else if (secondFighter.health <= 0) {
        resolve(firstFighter)
      }
    }

    const keyUpControl = e => {
      switch (e.code) {
        case controls.PlayerOneBlock:
          firstFighterBlockAvailable = false;
          break;
        case controls.PlayerTwoBlock:
          secondFighterBlockAvailable = false;
          break;
        case controls.PlayerOneAttack:
          playerAttack(
            firstFighter,
            secondFighter,
            secondFighterHealth,
            secondFighterLineHealth,
            firstFighterBlockAvailable,
            secondFighterBlockAvailable
          )
          break
        case controls.PlayerTwoAttack:
          playerAttack(
            secondFighter,
            firstFighter,
            firstFighterHealth,
            firstFighterLineHealth,
            secondFighterBlockAvailable,
            firstFighterBlockAvailable
          )
          break;
      }
      isWinner();
    }

    const keyDownControl = e => {
      switch (e.code) {
        case controls.PlayerOneBlock:
          firstFighterBlockAvailable = true;
          break;
        case controls.PlayerTwoBlock:
          secondFighterBlockAvailable = true;
          break;
      }
    }

    document.addEventListener('keydown', keyDownControl);
    document.addEventListener('keyup',keyUpControl);

  })
}

const useSuperAttack = (attacker, defender, fighterHealth, fighterLineHealth, superAttackAvailable) => {

  const coolDown = 10000;

  if (superAttackAvailable === 1) {
    if (firstFighterSuperAttackAvailable) {
      minusHealth(attacker, defender, fighterHealth, fighterLineHealth)
      firstFighterSuperAttackAvailable = false
      setTimeout(() => firstFighterSuperAttackAvailable = true, coolDown)
    }
  } else if (superAttackAvailable === 2) {
    if (secondFighterSuperAttackAvailable) {
      minusHealth(attacker, defender, fighterHealth, fighterLineHealth)
      secondFighterSuperAttackAvailable = false
      setTimeout(() => secondFighterSuperAttackAvailable = true, coolDown)
    }
  }
}

const minusHealth = (attacker, defender, fighterHealth, fighterLineHealth) => {
  defender.health -= getSuperPower(attacker)
  getFighterHealth(defender, fighterHealth, fighterLineHealth)
}

const playerAttack = (attacker, defender, fighterHealth,
                      fighterLineHealth, attackerFighterBlockAvailable, defenderFighterBlockAvailable) => {
  if (!attackerFighterBlockAvailable) {

    if(!defenderFighterBlockAvailable) {
      defender.health -= getDamage(attacker, defender)
    }

    getFighterHealth(defender, fighterHealth, fighterLineHealth)
  }
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  damage = Math.max(0, damage);
  return damage;
}

export function getSuperPower(fighter) {
  const power = fighter.attack * 2;
  return power;
}

export function getHitPower(fighter) {
  const power = fighter.attack * (1 + Math.random());
  return power;
}

export function getBlockPower(fighter) {
  const block =  fighter.defense * (1 + Math.random());
  return block;
}

function getFighterHealth(fighter, initialFighterHealth, lineHealth) {
  const result  = fighter.health / initialFighterHealth * 100;
  if (result > 0) {
    lineHealth.style.width = `${result}%`;
  } else {
    lineHealth.style.width = `0%`;
  }
  return result;
}

function useSuperAttackPress(callback, ...codes) {
  const pressed = new Set();

  const keyDownEvent = e => {
    pressed.add(e.code);

    const allKeys = codes.every(code => pressed.has(code));

    if (allKeys) {
      pressed.clear();
      callback();
    }
  }

  const keyUpEvent = e => pressed.delete(e.code);

  document.addEventListener('keydown', keyDownEvent);
  document.addEventListener('keyup', keyUpEvent);
}