import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {

  const img = createFighterImage(fighter)

  img.classList.add('winner-modal')

  showModal({
    title: `Winner is ${fighter.name}`,
    bodyElement: img,
  })
}
