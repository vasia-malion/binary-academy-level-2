import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    try {
      const endPoint = `details/fighter/${id}.json`;
      const fighter = await callApi(endPoint, 'GET');

      return fighter
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
